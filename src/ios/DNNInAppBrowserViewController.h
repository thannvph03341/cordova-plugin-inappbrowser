//
//  DNNInAppBrowserViewController.h
//  Now Health
//
//  Created by Diep Nguyen on 11/4/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DNNInAppBrowserViewController : UIViewController<UIWebViewDelegate>
@property (strong, nonatomic) UIWebView *wkWebview;
@property (strong, nonatomic) UIActivityIndicatorView *spiner;
@property (strong, nonatomic) NSString *closeString;
@property (strong, nonatomic) NSString *urlString;
@end

NS_ASSUME_NONNULL_END
