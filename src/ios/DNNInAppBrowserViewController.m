//
//  DNNInAppBrowserViewController.m
//  Now Health
//
//  Created by Diep Nguyen on 11/4/19.
//

#import "DNNInAppBrowserViewController.h"
#import <WebKit/WebKit.h>
#import <Cordova/CDVAppDelegate.h>

@interface DNNInAppBrowserViewController()


@end

@implementation DNNInAppBrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];


    self.spiner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    [self.spiner setHidesWhenStopped:true];
    [self.spiner setColor: UIColor.blackColor];

    self.wkWebview = [[UIWebView alloc] initWithFrame:[self app].window.screen.bounds];
    [self.wkWebview setDelegate:self];
    NSURL *nsurl=[NSURL URLWithString: self.urlString];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];

    [self.view addSubview:self.wkWebview];

    [self.wkWebview loadRequest:nsrequest];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 50, self.view.frame.size.width, 50)];
    bottomView.backgroundColor = [UIColor colorWithRed:69.0/255.0 green:32.0/255.0 blue:139.0/255.0 alpha:1];
    [self.view addSubview:bottomView];

    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 5, 50, 30)];
    [closeButton setTitle: self.closeString forState: UIControlStateNormal];
    [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeButton:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:closeButton];

    [self.view addSubview:self.spiner];
    self.spiner.center = self.view.center;
    [self.view bringSubviewToFront:bottomView];
}

-(void)closeButton:(id)sender{
    [[self app].viewController dismissViewControllerAnimated:true completion:nil];
}

- (CDVAppDelegate*) app
{
   return (CDVAppDelegate*) [[UIApplication sharedApplication] delegate];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self.spiner startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.spiner stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.spiner stopAnimating];
}

@end
